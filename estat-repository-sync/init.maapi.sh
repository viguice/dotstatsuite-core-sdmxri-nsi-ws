#!/bin/sh 

rm -rf maapi.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/maapi.net.git maapi.net.mirrored

#git remote set-url origin https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/maapi.net.git
git -C ./maapi.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored.git

git -C ./maapi.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./maapi.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./maapi.net.mirrored config --local --add remote.origin.mirror true

git -C ./maapi.net.mirrored config --local -l