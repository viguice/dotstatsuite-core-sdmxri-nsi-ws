#!/bin/sh 
export GIT_SSL_NO_VERIFY=true

Echo "-> authdb.sql.mirrored"
git -C ./authdb.sql.mirrored fetch
git -C ./authdb.sql.mirrored push

Echo "-> maapi.net.mirrored"
git -C ./maapi.net.mirrored fetch
git -C ./maapi.net.mirrored push

Echo "-> authorization.net.mirrored"
git -C ./authorization.net.mirrored fetch
git -C ./authorization.net.mirrored push

Echo "-> nsiws.net.mirrored"
git -C ./nsiws.net.mirrored fetch
git -C ./nsiws.net.mirrored push

Echo "-> dr.net.mirrored"
git -C ./dr.net.mirrored fetch
git -C ./dr.net.mirrored push

Echo "-> estat_sdmxsource_extension.net.mirrored"
git -C ./estat_sdmxsource_extension.net.mirrored fetch
git -C ./estat_sdmxsource_extension.net.mirrored push

Echo "-> msdb.sql.mirrored"
git -C ./msdb.sql.mirrored fetch
git -C ./msdb.sql.mirrored push

Echo "-> sdmxsource.net.mirrored"
git -C ./sdmxsource.net.mirrored fetch
git -C ./sdmxsource.net.mirrored push

Echo "-> sr.net.mirrored"
git -C ./sr.net.mirrored fetch
git -C ./sr.net.mirrored push
