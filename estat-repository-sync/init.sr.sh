#!/bin/sh 

rm -rf sr.net.mirrored

git clone --bare https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/sr.net.git sr.net.mirrored

git -C ./sr.net.mirrored remote set-url --push origin https://gitlab.com/sis-cc/eurostat-sdmx-ri/sr.net.mirrored.git

git -C ./sr.net.mirrored config --local --add remote.origin.fetch +refs/heads/*:refs/heads/* 
git -C ./sr.net.mirrored config --local --add remote.origin.fetch +refs/tags/*:refs/tags/*
git -C ./sr.net.mirrored config --local --add remote.origin.mirror true

git -C ./sr.net.mirrored config --local -l