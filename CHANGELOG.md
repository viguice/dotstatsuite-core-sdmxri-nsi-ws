# HISTORY

### Versioning
The Dotstat version of the NSIWS Docker image is built on top of the Eurostat [NSIWS image](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored).
In Gitlab and in the changelog the release version of the OECD specific NSIWS is indicated with the last digit in the version number of the image, e.g. v8.2.1.[X] is version 8.2.1 of the default NSIWS and [x] indicates the OECD specific release version.  

## v8.19.2
### Description


### NSIWS v8.19.2 Changelog
The changelog for the base NSIWS v8.19.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.19.2/CHANGELOG.md)

### Issues 
- [#461](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/461) Fixed issue with lastNObservations/firstNObservations parameters by enforcing ordering data when these parameters are used


----


## v8.19.1
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.19.1 Changelog
The changelog for the base NSIWS v8.19.1 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.19.1/CHANGELOG.md)

### Issues 
- [#453](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/453) Addressed vulnerability: System.Data.SqlClient vulnerable to SQL Data Provider Security Feature Bypass
- [#454](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/454) Addressed vulnerability: Microsoft.Data.SqlClient vulnerable to SQL Data Provider Security Feature Bypass


----


## v8.19.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.19.0 Changelog
The changelog for the base NSIWS v8.19.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.19.0/CHANGELOG.md)

### Issues 
- [#58](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/58) Removed the trigger from CI/CD pipeline to run the performance tests steps on merge
- [#389](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/389) Eliminated multiple re-readings of the config file to increase performance - SDMXRI-2252
- [#406](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/406) Applied fix to properly escape double quotes in text values at SDMX-JSON exports - SDMXRI-2296
- [#408](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/408) Extended default app.config with missing X-Range in allowed-headers - SDMXRI-2299
- [#412](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/412) Fixed the Json error "409 : Could not find code '' in codelist" when querying with 'asOf' and 'includeHistory' params - SDMXRI-2307
- [#418](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/418) Fixed bug of incorrect labels returned for some codes in the code list - SDMXRI-2357
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/424) Applied fix to properly escape tabs in text values in SDMX-JSON exports - SDMXRI-2363
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/426) Added 5 more ESTAT repositories to mirroring scripts
- [#430](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/430) Values of non-coded dimensions can be not included in dynamic availability constraints - SDMXRI-2373
- [#438](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/438) Removed `*` and `~` SQL filters on NSIWS data queries - SDMXRI-2402
- [#440](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/440) Removed ORDER BY for non paginated data requests - SDMXRI-2391
- [#444](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/444) Improved long running query for Cleanup Mappingsets - SDMXRI-2254
- [#445](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/445) Corrected the bug resulting to return dots instead of commas in textual values at SDMX-JSON exports - SDMXRI-2363
- [#447](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/447) Fixed the issue of certain dataflows are returning more data than allowed with the constraints - SDMXRI-2401


----

## v8.18.7
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.18.7 Changelog
The changelog for the base NSIWS v8.18.7 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.18.7/CHANGELOG.md)

### Issues 
- [#393](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/393) 'asOf' parameter should return a unique SDMX dataset - SDMXRI-2258
- [#357](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/357) Implement SDMX-CSV 2.0 `labels=name` option - SDMXRI-2182
- [#404](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/404) Exports in xml (default) with 'includeHistory=true' return references to DSD instead of DF - SDMXRI-2285
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/382) Make IEntity (mappingsets) management asynchronous - SDMXRI-2218
- [#398](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/398) Fix vulnerabilities of siscc/sdmxri-nsi-maapi image revealed by docker scout - SDMXRI-2215 - part 2 - maapi.net
- [#378](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/378) Avoid duplicate calls for mappingsets info in data requests with range header - SDMXRI-2220
- [#394](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/394) 'includeHistory' parameter should return extra columns for 'validFrom' and 'validTo' values in CSV v2.0 - SDMXRI-2256
- [#384](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/384) Extend SDMX CSV 2.0 readers to support DSD reference, SDMXRI-2223


----

## v8.18.4
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.18.4 Changelog
The changelog for the base NSIWS v8.18.4 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.18.4/CHANGELOG.md)

### Issues 
- [#85](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/85) Update of docker image description to avoid duplication of configuration documentation
- [#303](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/303) Only the value property should be used for non-coded components in the structure part of SDMX-JSON 2.0 data messages - SDMXRI-2217
- [#372](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/372) Non-SDMX data versioning (time machine) through new 'asOf' parameter - Data - SDMXRI-2213
- [#373](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/373) SDMX data versioning (time machine) through includeHistory parameter - Data - SDMXRI-2213
- [#376](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/376) Fix vulnerabilities of siscc/sdmxri-nsi-maapi image revealed by docker scout - part 1 - SDMXRI-2215 
- [#392](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/392) Updated nuget references to match with ESTAT v8.18.4 - SDMXRI-2222
- [#400](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/400) Fix of "Sequence contains no matching element" error - SDMXRI-2216


----

## v8.18.2
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.18.2 Changelog
The changelog for the base NSIWS v8.18.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.18.2/CHANGELOG.md)

### Issues 
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/230) Fixed "500 Internal Server Error" for availableconstraint query when no data - SDMXRI-2191
- [#280](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/280) Fixed the error occured when submitting HCL - SDMXRI-1968
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/306) Implemented support for restricted access to confidential or embargoed data - SDMXRI-2180
- [#332](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/332) Corrected malformed smdx.org url on NSI WS v8.13.0 using structurespecificdata query param - SDMXRI-2145
- [#350](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/350) Fixed the issue of codes returned in HCL in a different order than submitted - SDMXRI-2187
- [#357](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/357) Implemented SDMX-CSV 2.0 `labels=name` option - SDMXRI-2182
- [#365](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/365) Fixed the issue of available CC wrongly restraining time range by time period selection - SDMXRI-2183
- [#368](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/368) Upgraded version of Microsoft.AspNetCore.Http.Abstractions nuget to remediate a critical vulnerability - SDMXRI-2188
- [#374](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/374) Deployed NSI version 8.18.0 in DevOps - SDMXRI-2210
- [#377](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/377) Deployed NSI version 8.18.2 in DevOps - SDMXRI-2214
- [#587](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/587) Fixed issue at retrieval of availability content constraints


----

## v8.17.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.17.0 Changelog
The changelog for the base NSIWS v8.17.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.17.0/CHANGELOG.md)

### Issues 
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/76) Back-end docker security scan added
- [#244](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/244) In extractions, allow configuring specific characters as SDMX-CSV column & decimal separators per locale - SDMXRI-1970
- [#248](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/248) Add a new HTTP X-Level option to get the referential metadata only at the current level - SDMXRI-1924
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/277) Extend SDMX-XML v2.0 readers to support deletion action features - SDMXRI-1991
- [#284](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/284) Check AllowedCC for dimensions in ref metadata exports
- [#290](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/290) Incorrect timezone usage in ActuallCC time range and in start & end dates of time periods in SDMX-JSON - SDMXRI-1972
- [#299](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/299) Retrieve numerical intentionally missing measures - SDMXRI-2025
- [#300](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/300) updatedAfter feature (timestamp updated/inserted or deleted) for ref metadata - SDMXRI-2140
- [#301](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/301) Modify CSV 2.0 writer to support multiple datasets with different actions - SDMXRI-2006
- [#305](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/305) NSI SDMX-JSON data response has incomplete content for a dataflow with special characteristics - SDMXRI-2051
- [#309](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/309) Split NSI data retrieval queries for different tasks (normal; updatedAfter; includeHistory) - SDMXRI-2027
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/310) updatedAfter to return 'Replace' dataset instead of 'Merge/Append' - SDMXRI-2027
- [#314](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/314) Json data requests fail for dataflows without time period - SDMXRI-2051
- [#317](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/317) Observations cannot be retrieved that have no value for an optional constrained attribute - SDMXRI-2143
- [#319](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/319) Modify sdmxsource json writer to support delete instructions - SDMXRI-2023
- [#320](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/320) Properly handle uploaded content constraints with duplicated codes in a repeated concept - SDMXRI-2028
- [#323](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/323) Update textType Count as numeric - SDMXRI-2029
- [#324](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/324) SDMX-JSON data: Return definition of parents when child items have data, SDMXRI-2037
- [#325](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/325) Deploy NSI version 8.15.0 in DevOps
- [#326](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/326) Write access should not be required on the website's folder - SDMXRI-2040
- [#328](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/328) An allowed content constraint is returned as "Actual" when retrieving as stub - SDMXRI-2046
- [#330](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/330) Dataflow-level attribute returned twice
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/331) Deploy NSI version 8.15.1 in DevOps
- [#333](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/333) Improve performance of function to save the actual content constraint - SDMXRI-2137
- [#338](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/338) Incorrect CSV response of NSIWS for deleted dataflow attributes - SDMXRI-2138
- [#339](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/339) SDMX-JSON data: Return definition of parents when child items have data, part2 - SDMXRI-2136
- [#341](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/341) Missing ref metadata in extractions with Allowed CC, and non-executed deletes - SDMXRI-2155
- [#348](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/348) Populate the action column of csv v2 metadata response - SDMXRI-2157
- [#349](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/349) Json v2 metadata queries with updatedAfter returns only the 'Delete' dataset - 'Replace' is missing - SDMXRI-2141
- [#353](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/353) (CSV) extractions with updatedAfter miss attributes attached at higher levels than selection used in filter - SDMXRI-2147
- [#355](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/355) Deploy NSI version 8.16.0 in DevOps - SDMXRI-2150
- [#356](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/356) Support labels=both option in CSV downloads in unauthenticated mode - SDMXRI-2151
- [#358](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/358) SDMX-JSON data: Return definition of parents when child items have data, part3 (treatment of localised and non-localised HCL combinations) - SDMXRI-2157
- [#359](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/359) Change the NSIWS to report appropriately the intentionally missing value - SDMXRI-2163
- [#360](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/360) In SDMX-JSON, '~' resp. '*' for ref. metadata to be reported in keys - SDMXRI-2164
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/361) Deploy NSI version 8.17.0 in DevOps - SDMXRI-2168
- [#366](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/366) Change base docker image of 'sdmxri-nsi' from debian to alpine
- [#367](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/367) Change base docker image of 'sdmxri-nsi-maapi' from debian to alpine
- [#540](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/540) Querying for ref. metadata in JSON format fails - SDMXRI-2146
- [#993](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/993) Added 'csvfilewithlabels' format option

----

## v8.13.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.13.0 Changelog
The changelog for the base NSIWS v8.13.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.13.0/CHANGELOG.md)

### Issues 
- [#282](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/282) Fixed the issue with detail=allcompletestubs parameter doesn't returning all related artefact's annotations - SDMXRI-1947
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/293) Added alternative usage of 'X-Range' header for range requests - SDMXRI-1982
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/313) Deploy NSI version 8.13.0 - SDMXRI-2014
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS - SDMXRI-1954

----

## v8.12.2
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.12.2 Changelog
The changelog for the base NSIWS v8.12.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.12.2/CHANGELOG.md)

### Issues 
- [#164](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/164) Fixed bug of leftover mappingset data at DataFlow deletion + bug with message "Enlisting in Ambient transactions is not supported" - SDMXRI-1771 + SDMXRI-1834
- [#268](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/268) Fixed header and sub-folder issues in zipped responses - SDMXRI-1937
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/269) Fixed issue of failing deletion of structures and related artefacts on first attempt in DLM - SDMXRI-1943
- [#289](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/289) Deploy NSI version 8.12.2 (patch) - SDMXRI-1955

----

## v8.12.1
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.12.1 Changelog
The changelog for the base NSIWS v8.12.1 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.12.1/CHANGELOG.md)

### Issues 
- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/86) Error when creating StructureSet with ConceptSchemeMap - SDMXRI-1495
- [#88](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/88) Fixed exception at meta data related SQL queries when content constraint has restriction on an attribute - SDMXRI-1935
- [#128](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/128) Categorisations are not always deleted on first attempt - SDMXRI-1658
- [#133](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/133) Incorrect number of observations returned in the content-range header when LastNObservations parameter is applied - SDMXRI-1836
- [#196](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/196) Enable HTTP response compression for NSI - SDMXRI-1830
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/214) timeout issue when deleting or updating a HCL/Hierarchical codelist - SDMXRI-1829
- [#229](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/229) NSI WS should stop executing aborted/abandoned requests - SDMXRI-1858
- [#235](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/235) Dimension position: Error when uploading non-sequential positions - SDMXRI-1803
- [#235](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/235) One (freq-related) metadata too much for X-Level-upperOnly - SDMXRI-1832
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/237) Wrong behaviour of 'isMultiLingual' property - SDMXRI-1835
- [#239](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/239) DryIoc Exception in maapi.net tool v8.9.2 - SDMXRI-1842
- [#241](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/241) Incorrect SDMX-JSON localised names (e.g. when language is unmatched or value is empty) - SDMXRI-1845
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/242) Actual content constraints' validFrom and validTo properties to be exposed in UTC format - SDMXRI-1867
- [#243](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/243) Upgrade Eurostat components to .net core 6.0
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/245) Error "Enlisting in Ambient transactions is not supported." when deleting dataflows in DLM
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) Deploy NSI version 8.12.0 in DevOps + build files upgraded to SDK v6.0 + minor fixes on logs + workaround for config file issue
- [#252](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/252) Swagger issue when the service is hosted under a virtual directory - SDMXRI-1853
- [#258](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/258) JSON data returned without dim value ID (with edited non-final structures) - SDMXRI-1878
- [#262](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/262) Issue in time span filter - SDMXRI-1938
- [#263](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/263) Fix of error "Not Created: Codelist OECD:CL_UNIT_MEASURE (v1.0), Incorrect syntax near ')'."
- [#271](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/271) Added 'csvfile' format mapping to configuration file - SDMXRI-1936
- [#274](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/274) Add Merge Action to SDMX source, add CurrentAction to dataReaderEngine - SDMXRI-1934
- [#278](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/278) Extend SDMX-CSV v2.0 readers to support deletion action features - SDMXRI-1939 - NSI WS 8.12.1
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) Deploy NSI version 8.12.1 in DevOps
- [#349](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/349) Data download error: "Operations that change non-concurrent collections must have exclusive access" - SDMXRI-1872 
- Added defaultCultureInfo to config file as the default cultureinfo in docker container is Invariant leading to errors in some cases

----

## v8.9.2.2
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.9.2 Changelog
The changelog for the base NSIWS v8.9.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.9.2/CHANGELOG.md)

### Issues 
- [#41](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config/-/issues/41) Change categorisation/createStubCategory to false


## v8.9.2.1
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.9.2 Changelog
The changelog for the base NSIWS v8.9.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.9.2/CHANGELOG.md)

### Issues 
- [#232](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) add servicebus functionality (part 1) 


## v8.9.2
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.9.2 Changelog
The changelog for the base NSIWS v8.9.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.9.2/CHANGELOG.md)

### Issues 
- [#232](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/232) NSI Dissemination db health check 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/227) Corrected bugs in ref metadata implementation
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/222) SDMX-JSON: Hierarchy in Hierarchical Codelist (HCL) misses the ID (and links) - SDMXRI-1816
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/215) Fix failing JSON v1.0 unit tests in SdmxSource - SDMXRI-1812
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/212) SDMX-JSON V2 NSI accept header support - SDMXRI-1809
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/192) NSI data retriever metadata support - SDMXRI-1795 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/282) Metadata attributes mapping sets management - SDMXRI-1772 
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/183) Wrong data returned when querying for several frequencies - SDMXRI-1768
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/167) LastNObservations parameter returns first N observations - SDMXRI-1757
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/150) SDMX-CSV 2.0.0 (meta)data download - SDMXRI-1806
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/233 SDMX-JSON 2.0.0 (meta)data download - SDMXRI-1779
- [#](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Performance not acceptable when retrieving the first observation per seriesKey in a large dataflow - SDMXRI-1758


## v8.8.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.8.0 Changelog
The changelog for the base NSIWS v8.8.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.8.0/CHANGELOG.md)

### Issues 
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/165) Implement SDMX-CSV 2.0.0 data reader in SdmxSource - SDMXRI-1745 - NSI WS 8.8.0
- [#188](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/188) Error when upgrading MSDB from v6.14 to v6.17 - SDMXRI-1764
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/143) AfterPeriod and BeforePeriod in CCs must not be converted to StartPeriod and EndPeriod - SDMXRI-1687 - NS WS 8.8.0
- [#156](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/156) Inconsistent structure ID in structure-specific data messages - SDMXRI-1732 - NSI WS 8.8.0
- [#162](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/162) Wrong class name in URN when retrieving stub artefacts - SDMXRI-1748 - NSI WS 8.8.0

## v8.7.1
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.7.1 Changelog
The changelog for the base NSIWS v8.7.1 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.7.1/CHANGELOG.md)

### Issues 
- [#171](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/171) Fix of regression bug for usage of reserved keyword in SQL queries
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) Deploy NSI version 8.7.1 in DevOps
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/224) Allow retrieving data with constrained but non-provided optional attributes


## v8.5.0
### Description
[#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147 )  Servicebus functionality (part 1)

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.5.0 Changelog
The changelog for the base NSIWS v8.5.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.5.0/CHANGELOG.md)

### Issues 
- [#21](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/21) Support for non-calendar year reporting
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/81) Implement SDMX-JSON writer for StructureSets, MSD, Metadataflow and ProvisionAgreement
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/95) Incorrect HTTP status code returned for failing data queries
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/108) For "available" ContentConstraint replace ReferencePeriod by CubeRegion-TimeRange
- [#117](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/117) Further performance improvements for range requests
- [#121](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/121) Change NSIWS response code to 404, when mappingsets are missing
- [#134](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/134) AnnotationTitle is still limited to 70 characters when not attached to a dataflow (and all SDMX objects by extension) 
- [#137](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/137) Improve the successful message when updating a non-final codelist
- [#146](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/146) Escape dimension column in sql query for dynamic actual constraint
- [#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/147) Unable to upload non-final MSD
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/157) NSI uses wrong period end of the filter's endPeriod parameter & high-frequency values are ignored
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) Deploy NSI version 8.5.0 in DevOps
- [#216](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/216) NSI WS should take data database connection parameters from configuration


## v8.2.0
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

### NSIWS v8.2.0 Changelog
The changelog for the base NSIWS v8.2.0 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.2.0/CHANGELOG.md)
Please note that there is a typo in this change log and the actual MSDB version supported by NSIWS v8.2.0 is v6.14 (confirmed by ESTAT).

### Issues 
- [#73](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/73) StartDate of a ContentConstraintObjectCore instance can be changed to invalid value
- [#97](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/97) Treatments of afterPeriod and beforePeriod are mixed up in TimeRangeCore implementation
- [#119](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/119) Fix PIT related code in eurostat's maapi.net library
- [#136](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/136) Unclear error message for content constraint with missing referenced dataflow
- [#138](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/138) Make rest data retrievals asynchronous
- [#139](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/139) Deploy NSI version 8.2.0 in DevOps
- [#140](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/140) Improve performance when getting the Frequency dimension from a DSD

## v8.1.3
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and databases. 

### NSIWS v8.1.3 Changelog
The changelog for the base NSIWS v8.1.3 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.3/CHANGELOG.md)

### Issues 
- [#31](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/31) Incorrect sender ID in SDMX messages
- [#35](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/35) Improve error/status message for all structure updates
- [#98](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/98) Enhancement of content constraint structure message validation
- [#101](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/101) Not possible to update parent relations in a non-final codelist used in a DSD
- [#104](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/104) SdmxRegistryService URL configuration
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/105) Performance improvements for 0-0 range requests
- [#106](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/106) Fix issue of last N actually selecting first N periods
- [#107](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/107) Fix writing of NULLs in JSON data message
- [#109](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/109) Values of attributes at group attachment level not written in series node in sdmx-json
- [#112](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/112) Support for DSD without Time dimension (part 2 for appropriate SDMX-JSON export)
- [#129](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/129) Upgrade to NSI version 8.1.3

## v8.1.2.1 
### Description
The changelog for the base NSIWS v8.1.2 image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.2/CHANGELOG.md).

### Issues 
- [#37](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/37)  Authorization enabled by default. 
- [#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Add step to trigger performance tests in dotstatsuite-quality-assurance project. 
[#37](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/37)  Authorization enabled by default. 
<BR>[#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Add step to trigger performance tests in dotstatsuite-quality-assurance project. 

## v8.1.2 
### Description

==Important== Before using this package be sure to have backed up both your Data and Structure database.

This version is a major update with breaking changes to the both code and databases. 

### NSIWS v8.1.2 Changelog
The changelog for the base NSIWS image can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored/-/blob/8.1.2/CHANGELOG.md)

### Issues 
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/48) Remove the custom .Stat Suite NSI-plugin(Replace NSI-Plugin) 
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) Upgrade to NSI version 8.1.2 