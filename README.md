# Content of this repository
The content of this repository is used to produce two Docker images and published them to Docker Hub:
* 1 **[sdmxri-nsi](https://hub.docker.com/r/siscc/sdmxri-nsi)** image: The NSI web service (for structure upload/download) with the [vanilla version](https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/nsiws.net.oecd.git) ([SIS-CC mirror here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/nsiws.net.mirrored))
* 2 **[siscc/sdmxri-nsi-maapi](https://hub.docker.com/repository/docker/siscc/sdmxri-nsi-maapi)** image: Based on *siscc/sdmxri-nsi* image extended with templates + shell scripts for NSI configuration and with addition of the following two components:
  * The *maapi.net tool/utility* for initialization/upgrade of the mapping store database built from the [SIS-CC mirror](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored) of the [Eurostat repository](https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/maapi.net.git)
  * The *.Stat v8 specific authorization plugin* built from the [SIS-CC mirror](https://gitlab.com/sis-cc/eurostat-sdmx-ri/authorization.net.mirrored) of the [Eurostat repository](https://citnet.tech.ec.europa.eu/CITnet/stash/scm/sdmxri/authorization.net)

This repository does not contain code for any solution; it only contains recipes and scripts to support the creation of the Docker images.

The reason of why the .Stat Suite has a repository to publish Docker images for third party components is the following:
*  Currently Eurostat does not create Docker images of their components.
*  The .net solution containing the nsi web service and the maapi tool does not support completely configuration from different sources (including environment variables). 
    * This repository includes entry scripts that replace template placeholders in the app.config with values given by environment variables.
	

## Configuration parameters of *siscc/sdmxri-nsi-maapi* docker image

The configuration parameters of *siscc/sdmxri-nsi-maapi* image can be found on [Docker Hub](https://hub.docker.com/repository/docker/siscc/sdmxri-nsi-maapi)
	
---	
# Source code based installation of NSI web service with .Stat authorization plugin

## Installation steps

The [Source code Windows installation example of .Stat Core services](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/blob/master/content/install-source-code/windows-stat-core-services.md) document contains detailed description of source code based installation of NSI WS including the building of authorization plugin.

## Register connection string of mapping store database
In the NSI webservice config/app.config file inside the node "connectionStrings" in "configuration", add the connection string with the data space ID. 
E.g. for `design` data space add the  following line:
```xml
<configuration>
    <connectionStrings>
        ...
        <add name="design" connectionString="Data Source=STRUCTURE_DB_HOST;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX;" providerName="System.Data.SqlClient"/>
    </connectionStrings>
</configuration>
```

### Authentication/Authorization and release management (PIT) configuration

The prerequisites to setup Authentication/Authorization with NSI web service are following:

- OpenIdMiddlewareBuilder and UserAuthorizationRulesMiddlerwareBuilder middlewares are enabled in NSI main config file
- The authorization plugin related dll-s are built and copied into *Plugins* folder of NSI WS
- Auth configuration is defined either via environment variables or JSON configuration file. The JSON file can have any name with '.json' extension and should be placed into *config* folder, e.g.: /config/auth.json
- DotStatSuiteCoreCommonDbConnectionString connection string to a database with auth rules should be set as descibed in [this section](#keys)  
- Authorization rules are defined through [Authorization service](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management)

To enable OpenID middleware set **OpenIdMiddlewareBuilder** and **UserAuthorizationRulesMiddlerwareBuilder** under `<appSettings>` in `middlewareImplementation` of NSI webservice `app.config` file.

Example:

```xml
<add key="middlewareImplementation" value="CorsMiddlewareBuilder,OpenIdMiddlewareBuilder,LoggingOptionsBuilder,UserAuthorizationRulesMiddlerwareBuilder"/>
```

Auth configuration should be added to the file with a `json` extension inside a main `config` folder (for example /config/auth.json). Example configuration as a json object:

```json
{
  "auth": {
    "enabled": true,
    "allowAnonymous": true
    "authority": "AUTHORITY URL",
    "clientId": "CLIENT ID VALUE",
    "requireHttps": true,
    "validateIssuer": true,
    "showPii": false
  },
  "authorization": {
    "enabled":  true,
    "method": "dotstat",
    "PrincipalFrom":  "context"
  },
  "DotStatSuiteCoreCommonDbConnectionString": "Data Source=COMMON_DB_HOST;Initial Catalog=COMMON_DB_NAME;User ID=COMMON_DB_USERNAME;Password=COMMON_DB_PASSWORD;",
  "mappingStore": {
    "Id": {
      "Default": "design",
      "FromQueryParameter": "Never"
    }
  },
  "disseminationDbConnection": {
    "dbType": "SqlServer",
    "connectionString": "Data Source=DATA_DB_HOST;Initial Catalog=DATA_DB_NAME;User ID=DATA_DB_USERNAME;Password=DATA_DB_PASSWORD"
  },
  "enableReleaseManagement": false,
  "applyContentConstraintsOnDataQueries": true,
  "autoDeleteMappingSets": true,
  "useHierarchicalCodelists": true 
}
```

#### Authentication configuration

| Setting    | Description |
|------------|-------------|
| auth.enabled | Controls whether OpenIdMiddlewareBuilder is activated for OpenId authentication |
| auth.allowAnonymous | Enables anonymous access without JWT token (please note that corresponding authorization rule(s) must be created also, e.g. read structure and data for all users) |
| auth.authority | Authority url of token issuer, e.g. "http://keycloak:8080/auth/realms/demo" |
| auth.clientId | Client/application id, e.g "stat-suite" |
| auth.requireHttps | Is HTTPS connection to OpenID authority server required |
| auth.validateIssuer | Is iss (issuer) claim in JTW token should match the configured authority |
| auth.showPii | When set to TRUE outputs additional debug information in case of invalid token, which may contain personally identifiable information (PII) |
| auth.audience|The expected `aud` claim during token validation. If not set, it defaults to `clientId` |

#### Authorization configuration

| Setting    | Description |
|------------|-------------|
| authorization.enabled | 'true' - Determines if UserAuthorizationRulesMiddlerwareBuilder is activated for authorization. The default value for this docker image is `true`, which normally should not be changed. |
| authorization.method | `dotstat` - authorization uses .Stat Suite's authorization rules. The default value for this docker image is `dotstat`, which normally should not be changed. |
| authorization.PrincipalFrom | `context` - The principal used at authorization should be taken from context object.  The default value for this docker image is `context`, which normally should not be changed. |
| DotStatSuiteCoreCommonDbConnectionString | The connection string to .Stat Suite's Common database with authorization rules |
| mappingStore.Id.Default | the unique identifier of the data space of the NSI WS instance (also used at authorization rules), e.g. "design" |
| mappingStore.Id.FromQueryParameter | `Never` - A parameter controlling whether store id (i.e. data space id) could be provided in GET Query parameter. The default value for this docker image is `Never`, which normally should not be changed. |

#### PIT configuration

| Setting    | Description |
|------------|-------------|
| enableReleaseManagement | Controls if access to PIT (point-in-time) release data allowed on this NSI WS instance (please note that corresponding user authorization rule(s) must be created also) |

### Allowed content constraint support
| Setting    | Description |
|------------|-------------|
| applyContentConstraintsOnDataQueries | `true` - Specifies if NSI WS should apply allowed content constraints at data retrieval. The default value for this docker image is `true`, which normally should not be changed. |

### Automatic deletion of mapping sets when a dataflow is deleted
| Setting    | Description |
|------------|-------------|
| autoDeleteMappingSets | `true` - Specifies if NSI WS should also delete related mapping sets upon deletion of a dataflow. The default value for this docker image is `true`, which normally should not be changed. |


### Hierarchical Codelist parents output in JSON data message
| Setting    | Description |
|------------|-------------|
| useHierarchicalCodelists | `true` - Specifies if NSI WS should output Hierarchical Codelists (HCL) parents in JSON message, based on annotation (HIER_CONTEXT). See more [here](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/filters/advanced-hierarchies/) and [example DSD here](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/blob/master/content/TEST-DF_AREA-1.0-all.xml). The default value for this docker image is `true`, which normally should not be changed.


### Data database connection overriding the ones stored in [DB_CONNECTION] table

The "disseminationDbConnection" section in configuration is optional. 
Whenever it is used both `dbType` and `connectionString` values must be provided. 
If the data db connection string is present in the configuration, then all data queries target the database defined by the connection string, the NSI WS takes data database connection parameters from configuration instead of [DB_CONNECTION] table in structure database.
If the configuration contains no such parameter or the values are empty, the database connections in [DB_CONNECTION] table is used (default behaviour of NSI WS).

| Setting    | Description |
|------------|-------------|
| disseminationDbConnection.dbType | Type of the database must be one of the following values: "SqlServer", "Oracle", "MySQL" |
| disseminationDbConnection.connectionString | The connection string to a data dissemination database |

### Other configuration options
| Setting    | Description |
|------------|-------------|
| defaultCultureInfo | Sets the default culture of the service using a two-letter culture code. |
| enableResponseCompression | Specifies whether the response compression middleware of ASP.NET Core should be used to compress server responses. |
| datastructure.allowSdmx21CrossSectional | Controls the behavior of the Web Service conserning Cross Sectional SDMX v2.0 support in SDMX v2.1 Structure and data formats |
| categorisation__createStubCategory | Controls whether to create a stub Category for any missing categories when it imports a SDMX v2.1 Categorisation or SDMX v2.0 Dataflow/Metadataflow with CategoryReference | 
| useCultureSpecificColumnAndDecimalSeparators | Allows configuring specific characters as SDMX-CSV column & decimal separators per locale defined by the Accept-Language header of the request. The default value for this docker image is `true` |
| excludeNonCodedDimensionsDuringConstraintCalculation | Indicates if the NSIWS service should exclude the values found for non coded dimensions when querying for the Availability Content Constraints. The default value for this docker image is `false` |


### Logging configurarion
How to modify the default logging configuration? [See more.](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/blob/master/docs/readme.md)

Optionally, it's possible to log activity to [Google.Gloud](https://cloud.google.com/logging/docs) with *AutoLog2Google* = true. 
| Setting    | Description |
|------------|-------------|
| AutoLog2Google | Automatically log to google cloud. Default = false |
| GoogleLogLevel | One of (DEBUG,INFO,ERROR). Default = DEBUG |
| GoogleProjectId | Google.Cloud logging [projectId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#projectid), required only if AutoLog2Google=true |
| GoogleLogId | Google.Cloud logging [logId](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/configuration#logid). Default = DOTSTAT_BACKEND |
| GOOGLE_APPLICATION_CREDENTIALS | If the service is executed outside of Google Cloud Platform it is required to set this variable with a path to a service account JSON file for an authentication, see more [here](https://cloud.google.com/dotnet/docs/reference/Google.Cloud.Logging.Log4Net/latest/#authentication) |
